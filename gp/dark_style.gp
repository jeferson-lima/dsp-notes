set macros
white = 'textcolor rgb "white"'
set xlabel @white
set ylabel @white
set border lc rgb 'white'
set key @white
set grid lc 'white'
