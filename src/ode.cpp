#include<iostream>
#include<cmath>
#define N 200

double f(double x, double t);

using namespace std;

int main()
{
    double x = 3.0, h = 0.01, t = -1.0;
    double dx, d2x, d3x, d4x;
    cout << "#k \tT \tx"  << endl;
    cout << -1 << "\t" << t << "\t" << x << endl;
    for (int k = 0; k < N; k++)
    // {
    //     x = x + h * f(x,t);
    //     t = h + t;
    //     cout << k << "\t" <<
    //             t << "\t" <<
    //             x << endl;
    // }
    {
        dx = cos(t) - sin(x) + pow(t,2);
        d2x= -sin(t) - dx*cos(x) + 2*t;
        d3x= -cos(t) + - d2x*cos(x) + pow(dx,2)*sin(x)+ 2;
        d4x= sin(t) - d3x*cos(x) + 3*dx*d2x*sin(x) + pow(dx, 3)*cos(x);
        x  = x + h*(dx + h/2*(d2x +h/3*(d3x + h/4*(d4x))));
        t = t + h;
        cout << k << "\t" <<
                t << "\t" <<
                x << endl;
    }
    return 0;
}

double f(double x, double t)
{
    return cos(t) - sin(x) + pow(t,2);
}
