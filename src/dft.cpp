/* ----------------------------------------------------------------------------
 * Copyright 2024, Jeferson Lima
 * All Rights Reserved
 * See LICENSE for the license information
 * -------------------------------------------------------------------------- */

/**
 *  @file   dft.cpp
 *  @author Jeferson Lima
 *  @brief  Digital Fourier Transform
 *  *  @date   May 21, 2024
 **/
#include <iostream>
#include <cmath>
#include "ecg_noise_60hz_fs_360hz_3600_samples.h"

using namespace std;

double absComplexNumber(double real, double imag);
void ecgCopyArray(double* x, const double *ecg);
void genSignal(double* x);

int main(int argc, char** argv)
{
  double Xr[N];     //real part
  double Xi[N];     //imaginary part
  double x[N];

  ecgCopyArray(x,ecg_noise_60hz_fs_360hz_3600_samples);
  //  genSignal(x);

  cout << "#Freq \tDFT \tTime \tECG" << endl;
  for (int k = 0; k < static_cast<int>(N); k++) 
  {
    Xr[k] = 0;
    Xi[k] = 0;
    for (int n = 0; n < N; n++) 
    {
      Xr[k] = Xr[k] + x[n] * cos(2 * M_PI * k * n / N);
      Xi[k] = Xi[k] - x[n] * sin(2 * M_PI * k * n / N);
    }
    cout << static_cast<double>(k)/(N/2.0)*Fs/2.0 << "\t" <<
      2.0/N * absComplexNumber(Xr[k], Xi[k]) << "\t" <<
      static_cast<double>(k)/Fs << "\t" <<
      x[k] << endl;
  }
  return 0;
}


double absComplexNumber(double real, double imag)
{
  return sqrt(pow(real,2) + pow(imag,2) );
}

void ecgCopyArray(double* x, const double *ecg)
{
  for (int n=0; n < N; n++)
  {
    x[n] = ecg[n];
  }
}

void genSignal(double* x)
{
  const double F1 = 4.2, F2 = 13.2;
  for (int n=0; n < N; n++)
  {
    x[n] = 3.3 * sin(2 * M_PI * F1 * static_cast<double>(n)/Fs)
      + 2.1 * cos(2 * M_PI * F2 * static_cast<double>(n)/Fs);
  }
}
