/* ----------------------------------------------------------------------------
 * Copyright 2024, Jeferson Lima
 * All Rights Reserved
 * See LICENSE for the license information
 * -------------------------------------------------------------------------- */

/**
 *  @file   dc_motor_ex.cpp
 *  @author Jeferson Lima
 *  @brief  Digital Fourier Transform
 *  *  @date   May 21, 2024
 **/
#include <iostream>
#define N 2
#define STEPS 10000

void DCMotor(const double* x, double* dxdt, double t);

using namespace std;

int main()
{
  // initial condition
  double x[N] = {0,0}, dxdt[N];
  // sampling time
  double h = 0.001;
  std::cout << "#t \tx1 \tx2" << std::endl;
  for(int k = 0; k < STEPS; k++)
  {
    DCMotor(x, dxdt, static_cast<double>(k)*h);
    x[0] = x[0] + h*dxdt[0];
    x[1] = x[1] + h*dxdt[1];

    if ((k % 10) == 0)
    {
      std::cout <<
        static_cast<double>(k)*h << "\t" <<
        x[0] << "\t"<<
        x[1] << std::endl;
    }
  }
  return 0;
}


void DCMotor(const double* x, double* dxdt, double t)
{
  double J = 0.01, b = 0.1, K = 0.01, R = 1, L = 0.5;
  double V = 1;

  dxdt[0] = (-b*x[0] + K*x[1])/J;
  dxdt[1] = (-K*x[0] - R*x[1] + V)/L;
}
