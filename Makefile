#####################################################
# LaTeX Makefile
# Latex Compile and Clean
# Author: Jeferson Lima
#####################################################

TEXDIR = $(PWD)/content
OUTDIR = $(PWD)/public
BUILD  = $(PWD)/build
SRCDIR = $(PWD)/src
TEXFILES = $(wildcard $(TEXDIR)/*.tex)
SRCFILES = $(wildcard $(SRCDIR)/*.cpp)
PDFS = $(TEXFILES:$(TEXDIR)/%.tex=$(OUTDIR)/%.pdf)
SRCS = $(SRCFILES:$(SRCDIR)/%.cpp=$(BUILD)/%)
TEXFLAGS =  -synctex=1 -interaction=nonstopmode --shell-escape 
CFLAGS = -lstdc++ -lm -Iinclude

R='\033[0;31m'
G='\033[0;32m'
NC='\033[0m'

all: clean pdf

pdf: $(PDFS)
$(OUTDIR)/%.pdf: $(TEXDIR)/%.tex
	@mkdir -p $(OUTDIR)
	@echo -e ${G}"[INFO] - Processing $@"${NC}
	@pdflatex $(TEXFLAGS) -output-directory=$(OUTDIR) $< > /dev/null 2>&1
	@echo -e ${G}"[INFO] - build (1x)."${NC}
	@if grep -q 'gnuplot' $<; then \
		cd $(OUTDIR) && for script in *.gnuplot; do gnuplot "$$script"; done; \
	fi
	@echo -e ${G}"[INFO] - build gnuplot."${NC}
	@pdflatex $(TEXFLAGS) -output-directory=$(OUTDIR) $< > /dev/null 2>&1
	@echo -e ${G}"[INFO] - build (2x)."${NC}
	@pdflatex $(TEXFLAGS) -output-directory=$(OUTDIR) $< > /dev/null 2>&1
	@echo -e ${G}"[INFO] - build (3x)."${NC}
	@echo -e ${R}"[ERRORS] $(nodir $@)"${NC}
	@grep -B 4 -sn --color=auto --no-ignore-case -i "Error " $(basename $@).log
	@rm -f *.gnuploterrors

await:
	@ls $(TEXDIR)/*.tex | entr -d $(MAKE) pdf

build: $(SRCS)
$(BUILD)/%: $(SRCDIR)/%.cpp
	@mkdir -p $(BUILD)
	@echo "Compile $<"
	@gcc $(CFLAGS) $< -o $@
	@$@ > $@.data
#	@gnuplot -e 'plot "$@.data" with lines; pause 2'
 
rmout:
	@rm -f $(OUTDIR)/*.aux $(OUTDIR)/*.log $(OUTDIR)/*.out $(OUTDIR)/*.gnuplot \
		$(OUTDIR)/*.tex $(OUTDIR)/*.toc $(OUTDIR)/*.vrb $(OUTDIR)/*.nav \
		$(OUTDIR)/*.snm $(OUTDIR)/*fig*.pdf $(OUTDIR)/*.gz *.gnuploterrors
	@echo "clean project."

clean: rmout
	@rm -f $(OUTDIR)/*.pdf
	@rm -f $(BUILD)/*
